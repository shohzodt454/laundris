const resolveBrandName = offer => {
  return offer.model_modification.model_variant.model_generation.model_series
    .brand_name
}

const resolveModelName = offer => {
  return offer.model_modification.model_variant.model_generation.model_series
    .name
}

const resolveBodyType = offer => {
  return offer.model_modification.model_variant.name
}

export { resolveBrandName, resolveModelName, resolveBodyType }
