import Vue from 'vue'
import VueRouter from 'vue-router'
import i18n from '@/libs/i18n'
import store from '@/store'

import { isUserLoggedIn } from '@/auth/utils'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/home/Home.vue'),
      meta: {
        pageTitle: i18n.t('verticalNav.home'),
        breadcrumb: [
          {
            text: i18n.t('verticalNav.home'),
            active: true
          }
        ]
      }
    },
    {
      path: '/customers',
      name: 'customers',
      component: () => import('@/views/customers/customers-list/ListCustomers.vue'),
      meta: {
        pageTitle: i18n.t('verticalNav.customers'),
        breadcrumb: [
          {
            text: i18n.t('verticalNav.customers'),
            active: true
          }
        ]
      }
    },
    {
      path: '/inventory',
      name: 'inventory',
      component: () => import('@/views/inventory/inventory-list/InventoryList.vue'),
      meta: {
        pageTitle: i18n.t('verticalNav.inventory'),
        breadcrumb: [
          {
            text: i18n.t('verticalNav.inventory'),
            active: true
          }
        ]
      }
    },
    {
      path: '/snapshot',
      name: 'snapshot',
      component: () => import('@/views/snapshots/snapshots-list/SnapshotsList.vue'),
      meta: {
        pageTitle: i18n.t('verticalNav.snapshot'),
        breadcrumb: [
          {
            text: i18n.t('verticalNav.snapshot'),
            active: true
          }
        ]
      }
    },
    {
      path: '/hotel-profile',
      name: 'hotel-profile',
      component: () => import('@/views/hotel/hotel-profile/HotelProfile.vue'),
      meta: {
        pageTitle: i18n.t('verticalNav.hotel'),
        breadcrumb: [
          {
            text: i18n.t('verticalNav.hotel'),
            active: true
          }
        ]
      }
    },
    {
      path: '/reservations',
      name: 'reservations',
      component: () => import('@/views/reservations/reservations-list/ReservationsList.vue'),
      meta: {
        pageTitle: i18n.t('verticalNav.reservations'),
        breadcrumb: [
          {
            text: i18n.t('verticalNav.reservations'),
            active: true
          }
        ]
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue'),
      meta: {
        layout: 'full'
      }
    },
    {
      path: '/error-404',
      name: 'error-404',
      component: () => import('@/views/error/Error404.vue'),
      meta: {
        layout: 'full'
      }
    },
    {
      path: '*',
      redirect: 'error-404'
    }
  ]
})

router.beforeEach((to, from, next) => {
  const isAuthenticated = store.getters['auth/isAuthenticated']

  if (to.name == 'login' && isAuthenticated) next({ name: 'home' })
  if (to.name != 'login' && !isAuthenticated) next({ name: 'login' })
  else next()
})

// ? For splash screen
// Remove afterEach hook if you are not using splash screen
router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
  if (appLoading) {
    appLoading.style.display = 'none'
  }
})

export default router
