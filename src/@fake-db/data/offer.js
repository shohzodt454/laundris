import mock from '@/@fake-db/mock'
import { paginateArray, sortCompare } from '@/@fake-db/utils'
import {
  resolveBrandName,
  resolveModelName
} from '@/utils/functions/offer-utils'

/* eslint-disable global-require */
const data = {
  offers: [
    {
      id: 70,
      model_package: null,
      model_modification: {
        name: '3.5 AT 4WD (281 л.с.)',
        engine_volume: 3471,
        engine_volume_unit: 'см3',
        engine_power: 281,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'FULL PLUGGABLE',
        model_variant: {
          name: 'Кроссовер 5-дв.',
          body_type: 'Кроссовер 5-дв.',
          model_generation: {
            name: '1 поколение [рестайлинг]',
            model_series: {
              name: 'Crosstour',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'Silver',
        color_code: '#AAA9AD'
      },
      photos: [
        {
          id: 277,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/infiniti.jpg',
          tag: 'car'
        },
        {
          id: 278,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/impala.jpg',
          tag: 'car'
        },
        {
          id: 279,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/impala2.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2013,
      price: '1500.00',
      mileage: 15000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.'
    },
    {
      id: 69,
      model_package: null,
      model_modification: {
        name: '3.2 MT (280 л.с.)',
        engine_volume: 3179,
        engine_volume_unit: 'см3',
        engine_power: 280,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'MECHANICAL',
        wheel_drive_type: 'BACK',
        model_variant: {
          name: 'Тарга',
          body_type: 'Тарга',
          model_generation: {
            name: '2 поколение',
            model_series: {
              name: 'NSX',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'yellow',
        color_code: '#FFDC64'
      },
      photos: [
        {
          id: 275,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/honda-nsx.jpg',
          tag: 'car'
        },
        {
          id: 276,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/honda-nsx2.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2005,
      price: '4500.00',
      mileage: 15000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Andijon',
      model_package_description: null,
      description:
        'На фотографиях не должно быть контактов, надписей, людей, водяных знаков и любых посторонних предметов.'
    },
    {
      id: 59,
      model_package: null,
      model_modification: {
        name: '2.0 T AT (211 л.с.)',
        engine_volume: 1991,
        engine_volume_unit: 'см3',
        engine_power: 211,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'BACK',
        model_variant: {
          name: 'Седан',
          body_type: 'Седан',
          model_generation: {
            name: '1 поколение',
            model_series: {
              name: 'Q50',
              brand_name: 'Infiniti'
            }
          }
        }
      },
      color: {
        color_name: 'Gas White',
        color_code: '#FFFFFF'
      },
      photos: [
        {
          id: 241,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/1.jpg',
          tag: 'Fuck you bitch'
        },
        {
          id: 242,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/2.jpeg',
          tag: 'Fuck you okay'
        },
        {
          id: 243,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/1.jpg',
          tag: 'Fuck you bitch'
        },
        {
          id: 244,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/2.jpeg',
          tag: 'Fuck you okay'
        }
      ],
      vin_code: null,
      production_year: 2014,
      price: '6500.00',
      mileage: 1000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description: 'This is very very very very good'
    },
    {
      id: 60,
      model_package: null,
      model_modification: {
        name: '2.0 T AT (211 л.с.)',
        engine_volume: 1991,
        engine_volume_unit: 'см3',
        engine_power: 211,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'BACK',
        model_variant: {
          name: 'Седан',
          body_type: 'Седан',
          model_generation: {
            name: '1 поколение',
            model_series: {
              name: 'Q50',
              brand_name: 'Infiniti'
            }
          }
        }
      },
      color: {
        color_name: 'Gas White',
        color_code: '#FFFFFF'
      },
      photos: [
        {
          id: 245,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/1.jpg',
          tag: 'Fuck you bitch'
        },
        {
          id: 246,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/2.jpeg',
          tag: 'Fuck you okay'
        }
      ],
      vin_code: null,
      production_year: 2014,
      price: '6500.00',
      mileage: 1000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description: 'This is very very very very good'
    },
    {
      id: 65,
      model_package: null,
      model_modification: {
        name: '1.6 MT (112 л.с.)',
        engine_volume: 1590,
        engine_volume_unit: 'см3',
        engine_power: 112,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'MECHANICAL',
        wheel_drive_type: 'FRONT',
        model_variant: {
          name: 'Седан',
          body_type: 'Седан',
          model_generation: {
            name: 'HW',
            model_series: {
              name: 'Concerto',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'Night Black',
        color_code: '#000000'
      },
      photos: [
        {
          id: 254,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/honda-concreto2.jpg',
          tag: 'car'
        },
        {
          id: 255,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/honda-concreto3.jpg',
          tag: 'car'
        },
        {
          id: 256,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/honda-concreto.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 1992,
      price: '12000.00',
      mileage: 15000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'хороший японец. Авто с Гуром и комфортное для города.По запчастям недорогой.Реальная иномарка по цене жигулей. Рекомендую.Машиной остался доволен.'
    },
    {
      id: 58,
      model_package: null,
      model_modification: {
        name: 'Q45 AT (282 л.с.)',
        engine_volume: 4494,
        engine_volume_unit: 'см3',
        engine_power: 282,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'BACK',
        model_variant: {
          name: 'Седан',
          body_type: 'Седан',
          model_generation: {
            name: '1 поколение',
            model_series: {
              name: 'Q45',
              brand_name: 'Infiniti'
            }
          }
        }
      },
      color: {
        color_name: 'Blue',
        color_code: '#0A05FF'
      },
      photos: [
        {
          id: 238,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car7.jpg',
          tag: 'car'
        },
        {
          id: 239,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car8.webp',
          tag: 'car'
        },
        {
          id: 240,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car9.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 1991,
      price: '15000.00',
      mileage: 151551,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Andijon',
      model_package_description: null,
      description:
        'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.'
    },
    {
      id: 66,
      model_package: null,
      model_modification: {
        name: '0.7 MT (64 л.с.)',
        engine_volume: 656,
        engine_volume_unit: 'см3',
        engine_power: 64,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'MECHANICAL',
        wheel_drive_type: 'BACK',
        model_variant: {
          name: 'Родстер',
          body_type: 'Родстер',
          model_generation: {
            name: '1 поколение',
            model_series: {
              name: 'Beat',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'yellow',
        color_code: '#FFDC64'
      },
      photos: [
        {
          id: 257,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/article3.jpg',
          tag: 'car'
        },
        {
          id: 258,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car4.webp',
          tag: 'car'
        },
        {
          id: 259,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car6.jpg',
          tag: 'car'
        },
        {
          id: 260,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car1.webp',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 1994,
      price: '15000.00',
      mileage: 15000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Bukhara',
      model_package_description: null,
      description:
        'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.'
    },
    {
      id: 61,
      model_package: null,
      model_modification: {
        name: 'QX56 AT 4WD (320 л.с.)',
        engine_volume: 5552,
        engine_volume_unit: 'см3',
        engine_power: 320,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'FULL',
        model_variant: {
          name: 'Внедорожник',
          body_type: 'Внедорожник',
          model_generation: {
            name: '2 поколение',
            model_series: {
              name: 'Модель QX-серии',
              brand_name: 'Infiniti'
            }
          }
        }
      },
      color: {
        color_name: 'Blue',
        color_code: '#0A05FF'
      },
      photos: [
        {
          id: 247,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/reader1.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2010,
      price: '15000.00',
      mileage: 15000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Bukhara',
      model_package_description: null,
      description:
        'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.'
    },
    {
      id: 68,
      model_package: null,
      model_modification: {
        name: '2.2 TDI MT (140 л.с.)',
        engine_volume: 2204,
        engine_volume_unit: 'см3',
        engine_power: 140,
        engine_power_unit: 'л.с.',
        engine_power_type: 'DIESEL',
        gear_box_type: 'MECHANICAL',
        wheel_drive_type: 'FRONT',
        model_variant: {
          name: 'Универсал',
          body_type: 'Универсал',
          model_generation: {
            name: '7 поколение',
            model_series: {
              name: 'Accord',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'Blue',
        color_code: '#0A05FF'
      },
      photos: [
        {
          id: 273,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/HondaAccord4.jpg',
          tag: 'car'
        },
        {
          id: 274,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/HondaAccord3.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2003,
      price: '15004.00',
      mileage: 15000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.'
    },
    {
      id: 63,
      model_package: null,
      model_modification: {
        name: '3.5 AT (213 л.с.)',
        engine_volume: 3471,
        engine_volume_unit: 'см3',
        engine_power: 213,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'FRONT',
        model_variant: {
          name: 'US-spec минивэн 5-дв.',
          body_type: 'US-spec минивэн 5-дв.',
          model_generation: {
            name: '2 поколение',
            model_series: {
              name: 'Odyssey',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'Silver',
        color_code: '#AAA9AD'
      },
      photos: [
        {
          id: 249,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/fc3af23fd7c4d89ca0300318bbb6d05f.jpg',
          tag: 'car'
        },
        {
          id: 250,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/Honda-Odyssey-2021-1600-15.jpg',
          tag: 'car'
        },
        {
          id: 251,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/minivjen-honda-odyssey-2021-nachinaetsja-s-32-910-dollarov-a-komplektacija-touring.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2000,
      price: '15600.00',
      mileage: 15000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'Для североамериканского рынка Honda Odyssey , известный в Японии как LaGreat (ラ グ レ イ ト, Ragureito ), представляет собой минивэн, производимый и продаваемый японским автопроизводителем Honda с 1994 года, теперь это пятое поколение, которое началось в 2018 году. Первоначально Odyssey был задуман и спроектирован в Японии после экономического кризиса 1990-х годов, который, в свою очередь, наложил серьезные ограничения на размер автомобиля и общую концепцию, обуславливая производство минивэна на существующем предприятии с минимальной модификацией. . Результатом стал минивэн меньшего размера в классе компактвэн , который был хорошо принят на внутреннем рынке Японии и менее принят в Северной Америке. Odyssey первого поколения продавался в Европе как Honda Shuttle. Honda Odyssey (Северная Америка) - https://ru.qaz.wiki/wiki/Honda_Odyssey_(North_America)'
    },
    {
      id: 62,
      model_package: null,
      model_modification: {
        name: '2.2 D MT (5 мест) (163 л.с.)',
        engine_volume: 2231,
        engine_volume_unit: 'см3',
        engine_power: 163,
        engine_power_unit: 'л.с.',
        engine_power_type: 'DIESEL',
        gear_box_type: 'MECHANICAL',
        wheel_drive_type: 'FRONT',
        model_variant: {
          name: 'Кроссовер',
          body_type: 'Кроссовер',
          model_generation: {
            name: '1 поколение [2-й рестайлинг]',
            model_series: {
              name: 'Captiva',
              brand_name: 'Chevrolet'
            }
          }
        }
      },
      color: {
        color_name: 'Night Black',
        color_code: '#000000'
      },
      photos: [
        {
          id: 248,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/infiniti-ex35.jpeg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2014,
      price: '24000.00',
      mileage: 13000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.\nНа фотографиях не должно быть контактов, надписей, людей, водяных знаков и любых посторонних предметов.'
    },
    {
      id: 71,
      model_package: null,
      model_modification: {
        name: '2.0 AT (155 л.с.)',
        engine_volume: 1998,
        engine_volume_unit: 'см3',
        engine_power: 155,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'FRONT',
        model_variant: {
          name: 'Седан 4-дв.',
          body_type: 'Седан 4-дв.',
          model_generation: {
            name: '7 поколение [рестайлинг]',
            model_series: {
              name: 'Accord',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'Gas White',
        color_code: '#FFFFFF'
      },
      photos: [
        {
          id: 280,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/faryi-i-bamper-Honda-Akkord-2018.jpg',
          tag: 'car'
        },
        {
          id: 281,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/2018_Honda_Accord_12.17.17.jpg',
          tag: 'car'
        },
        {
          id: 282,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/cbf12a5s-960.jpg',
          tag: 'car'
        },
        {
          id: 283,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/unnamed.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2008,
      price: '100000.00',
      mileage: 100000,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'КАСКАД-АВТО - официальный дилер Mercedes-Benz\nпредлагает Вашему вниманию:\nMercedes-Benz GLE 300 d 4MATIC Sport, 2021 года выпуска \n\nВы можете воспользоваться финансовыми программами от Мерседес-Бенц Банка:\n- приобрести авто в кредит по ставке от 4,4% годовых \n- приобрести авто в лизинг \n- оформить КАСКО от 3,5% от стоимости авто\n4 ГОДА ГАРАНТИИ!\n\n\nКОМПЛЕКТАЦИЯ: \nПодготовка для навигационных сервисов\nИнструкция по эксплуатации и сервисный буклет на Русском языке\nПодготовка для сервиса: дистанционный контроль над автомобилем\nПодготовка для сервиса: мониторинг автомобиля\nПакет интеграции смартфона\nПодготовка для Mercedes-Benz Link\nApple CarPlay\nAndroid Auto\nКамера заднего вида\nАктивный парковочный ассистент включая PARKTRONIC\nПереднее пассажирское сиденье с функцией памяти\nВнутр. и наруж. зеркало задн.вида на стороне водителя с автом. затемн.\nСистема экстренного торможения\nПодготовка для сервиса: дистанционный запуск двигателя\nGPS антенна\nЭлектрическая регулировка сиденья водителя с функцией памяти\nСистема экстренного вызова\nРасширенные функции MBUX\nКоммуникационный модуль\nНавигационная система на жестком диске\nПодготовка для сервиса: дорожная информация Live Traffic\nАвтоматическая коробка передач 9G-TRONIC\nМультифункциональное рулевое колесо с подогревом\nСистема контроля давления в шинах\nЗащита картера двигателя\nХодовая часть со стальной подвеской и пониженным клиренсом\nРешётка радиатора белого цвета\nСкладные боковые зеркала с электроприводом\n3-х летнее бесплатное обновление навигационных карт\nЗащита при транспортировке\nМультимедийная система MBUX\nПротивоугонная система\nОбивка потолка тканью цвета "Бежевый макиато"\nАвтоматическая система климат-контроля THERMATIC, 2 зоны\nОсвещение окружения автомобиля с проекцией логотипа\nАвтомобиль локальной сборки CKD Россия\nСветодиодные фары High Performance\nОтсутствие аптечки\nУпаковка отправляемых автомобилей\nДокатка\nПодготовка для спутниковой противоугонной системы MB\nГоризонтальная шторка багажного отделения EASY PACK\nДополнительные разъемы USB\nСтайлинг AMG\nПодготовка для цифрового радио\nДифференциальный датчик хода BAS\nДополнительное оборудование для стран с холодным климатом\nКокпит с широкоэкранными дисплеями\nФункция HANDS-FREE ACCESS\nПодогрев передних сидений\nСистема омывателя ветрового стекла с подогревом\nКомфортная подсветка салона Ambient lighting\nСигнализация проникновения в салон\nСистема KEYLESS-GO\nКрышка багажника EASY-PACK\nСистема удержания детей i-Size\nХром-пакет "Интерьер"\nТопливный бак\nТехнология очистки выбросов EURO 6\nVIN-номер под лобовым стелком\nSTEUERCODE RADDURCHMESSER MITTEL (GR. 790)\nАдаптация ходовой части для плохих дорог\nИнструмент для замены запасного колеса\nДекоративные элементы: дуб цвета "Антрацит", с пористой структурой\nМультифункциональное спортивное рулевое колесо с отделкой кожей наппа\nROHBAULASTSTUFE 0\nКомфорт-пакет KEYLESS-GO\nИнтерьер EXCLUSIVE\nЭкстерьер AMG Line\nПарковочный пакет\nПакет зеркал\nПакет устройств защиты от кражи\nПакет памяти\nCKD Konfiguration 3\nЛетние шины\nЛегкосплавные колёсные диски AMG 50,8 см (20") с 5 сдвоенными спицами\nSTC solid top\nИндикация состояния задних ремней безопасности на дисплее комбинации п\nПанель приборов и подоконная линия дверей из искусственной кожи АРТИКО\nАвтоматическое отключение подушки безопасности переднего пассажира\nВелюровые коврики\nФункция дополненной реальности для навигационной системы MBUX\nПоясничная опора с регулировкой в 4-х направлениях\nСистема BlueTEC вкл. ёмкость для жидкости AdBlue®\n\nВ нашем дилерском центре действует программа TRADE-IN (обмен Вашего старого авто на новый).\nЗа более подробной информацией обращайтесь в отдел продаж.'
    },
    {
      id: 67,
      model_package: null,
      model_modification: {
        name: '2.0 AT 4WD (156 л.с.)',
        engine_volume: 1998,
        engine_volume_unit: 'см3',
        engine_power: 156,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'FULL',
        model_variant: {
          name: 'Минивэн',
          body_type: 'Минивэн',
          model_generation: {
            name: '1 поколение [рестайлинг]',
            model_series: {
              name: 'Edix',
              brand_name: 'Honda'
            }
          }
        }
      },
      color: {
        color_name: 'Blue',
        color_code: '#0A05FF'
      },
      photos: [
        {
          id: 261,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/article1.jpg',
          tag: 'car'
        },
        {
          id: 262,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car8.webp',
          tag: 'car'
        },
        {
          id: 263,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car7.jpg',
          tag: 'car'
        },
        {
          id: 264,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/honda-concreto2.jpg',
          tag: 'car'
        },
        {
          id: 265,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/honda-concreto.jpg',
          tag: 'car'
        },
        {
          id: 266,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car6.jpg',
          tag: 'car'
        },
        {
          id: 267,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car5.jpg',
          tag: 'car'
        },
        {
          id: 268,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/article3.jpg',
          tag: 'car'
        },
        {
          id: 269,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car9.jpg',
          tag: 'car'
        },
        {
          id: 270,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car10.jpg',
          tag: 'car'
        },
        {
          id: 271,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/car11.jpg',
          tag: 'car'
        },
        {
          id: 272,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/carousel1.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2007,
      price: '123212.00',
      mileage: 45645,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.'
    },
    {
      id: 64,
      model_package: null,
      model_modification: {
        name: '3.0 AT 4WD (264 л.с.)',
        engine_volume: 2994,
        engine_volume_unit: 'см3',
        engine_power: 264,
        engine_power_unit: 'л.с.',
        engine_power_type: 'PETROL',
        gear_box_type: 'AUTOMATIC',
        wheel_drive_type: 'FULL PLUGGABLE',
        model_variant: {
          name: 'Кроссовер',
          body_type: 'Кроссовер',
          model_generation: {
            name: '2 поколение',
            model_series: {
              name: 'Equinox',
              brand_name: 'Chevrolet'
            }
          }
        }
      },
      color: {
        color_name: 'Red',
        color_code: '#EC2623'
      },
      photos: [
        {
          id: 252,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/chevrolet_equinox_1.jpg',
          tag: 'car'
        },
        {
          id: 253,
          photo:
            'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/chevrolet_equinox_2.jpg',
          tag: 'car'
        }
      ],
      vin_code: null,
      production_year: 2010,
      price: '241425.00',
      mileage: 1500,
      mileage_unit: 'KILOMETERS',
      offer_city: 'Tashkent',
      model_package_description: null,
      description:
        'Отличный семейный кроссовер. Перед покупкой пересмотрел кучу вариантов. Хотелось комфорта, просторного салона, мягкой подвески, простой атмосферник и обычный гидротрансформатор. Выбирал между: Jeep Cherokee, Ford Kuga/Escape, Jeep renegade, Nissan rogue/x-trail, Mitsubishi Outlander XL, Subaru forester и ещё много других в диапазоне 12-14 тыс. По обзорам на Ютуб ничего не было понятно до того как сам не прокатился на каждом из этих авто. И какое же было мое удивление когда Equinox просто уделал их всех. Из него просто не хотелось выходить. Прочитав ещё кучу отзывов и обзоров было принято решение брать. В салоне места как у крузака. Шумка отличная - двойные стеклопакеты. Ямки на наших дорогах глотает очень уверенно. И очень удивила динамика при обгоне в хорошем смысле, 150км/ч держит дорогу без дерганий. Переднего привода з головой хватило зимой. Много кто пишет про низкую губу переднего бампера - за год ни разу не черкнул. Думал что будут проблемы с деталями - нашел все что нужно. Может разве что с кузовными, но пока к счастью не сталкивался.\nВ обще рекомендую, за свои деньги топчик.'
    }
  ]
}
/* eslint-enable */

// ------------------------------------------------
// GET: Return Users
// ------------------------------------------------
mock.onGet('/offer/offers').reply(config => {
  // eslint-disable-next-line object-curly-newline
  const {
    page = 1,
    status = null,
    ordering = 'id',
    page_size = 10,
    search = '',
    model_series = null,
    brand = null,
    offers = null
  } = config.params
  /* eslint-enable */

  const queryLowered = search.toLowerCase()
  let filteredData = []

  // ? if offers is provided then return selected offers
  if (Array.isArray(offers)) {
    filteredData = data.offers.filter(t => offers.indexOf(t.id))
  } else {
    filteredData = data.offers.filter(
      offer =>
        /* eslint-disable operator-linebreak, implicit-arrow-linebreak */
        (resolveBrandName(offer)
          .toLowerCase()
          .includes(queryLowered) ||
          resolveModelName(offer)
            .toLowerCase()
            .includes(queryLowered)) &&
        offer.status === (status || offer.status)
    )
  }

  /* eslint-enable  */

  const sortedData = filteredData.sort(sortCompare(ordering))
  if (ordering[0] == '-') sortedData.reverse()

  return [
    200,
    {
      results: paginateArray(sortedData, page_size, page),
      count: filteredData.length
    }
  ]
})

// ------------------------------------------------
// GET: Return Single offer
// ------------------------------------------------
mock.onGet(/\/offer\/offers\/\d+/).reply(config => {
  // Get event id from URL
  let offerId = config.url.substring(config.url.lastIndexOf('/') + 1)

  // Convert Id to number
  offerId = Number(offerId)

  const offerIndex = data.offers.findIndex(e => e.id === offerId)
  const offer = data.offer[offerIndex]

  if (offer) return [200, offer]
  return [404]
})

// ------------------------------------------------
// POST: Update Collection
// ------------------------------------------------

mock.onPut(/\/offer\/offers\/\d+/).reply(config => {
  // Get event from post data
  const { offer: offerData } = JSON.parse(config.data)

  // Convert Id to number
  offerData.id = Number(offerData.id)

  const offer = data.offers.find(e => e.id === Number(offerData.id))

  Object.assign(offer, offerData)

  return [201, { offer }]
})
