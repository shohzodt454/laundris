import mock from '@/@fake-db/mock'
import { paginateArray, sortCompare } from '@/@fake-db/utils'

/* eslint-disable global-require */
const data = {
  collections: [
    {
      id: 1,
      name: '10 автомобилей в необычном цвете',
      title: '10 автомобилей в необычном цвете',
      collection_id: 'unordinary',
      photo: 'https://ams3.digitaloceanspaces.com/avtovin/media/mustang.jpeg',
      misc: null,
      order: 0,
      car_offers: [
        {
          id: 70,
          model_package: null,
          model_modification: {
            name: '3.5 AT 4WD (281 л.с.)',
            engine_volume: 3471,
            engine_volume_unit: 'см3',
            engine_power: 281,
            engine_power_unit: 'л.с.',
            engine_power_type: 'PETROL',
            gear_box_type: 'AUTOMATIC',
            wheel_drive_type: 'FULL PLUGGABLE',
            model_variant: {
              name: 'Кроссовер 5-дв.',
              body_type: 'Кроссовер 5-дв.',
              model_generation: {
                name: '1 поколение [рестайлинг]',
                model_series: {
                  name: 'Crosstour',
                  brand_name: 'Honda'
                }
              }
            }
          },
          color: {
            color_name: 'Silver',
            color_code: '#AAA9AD'
          },
          photos: [
            {
              id: 277,
              photo:
                'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/infiniti.jpg',
              tag: 'car'
            },
            {
              id: 278,
              photo:
                'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/impala.jpg',
              tag: 'car'
            },
            {
              id: 279,
              photo:
                'https://ams3.digitaloceanspaces.com/avtovin/media/images/offers/impala2.jpg',
              tag: 'car'
            }
          ],
          vin_code: null,
          production_year: 2013,
          price: '1500.00',
          mileage: 15000,
          mileage_unit: 'KILOMETERS',
          offer_city: 'Tashkent',
          model_package_description: null,
          description:
            'Не указывайте в этом поле электронную почту, номер телефона, цену, адрес места осмотра и не предлагайте какие-либо услуги — такое объявление не пройдет модерацию.'
        }
      ]
    },
    {
      id: 2,
      name: '5 растаможенных электромобилей',
      title: '5 растаможенных электромобилей',
      collection_id: 'rastamojka',
      photo: 'https://ams3.digitaloceanspaces.com/avtovin/media/mustang.jpeg',
      misc: null,
      order: 0,
      car_offers: []
    },
    {
      id: 3,
      name: 'Редкие и экзотические автомобили',
      title: 'Редкие и экзотические автомобили',
      collection_id: 'exotic',
      photo: 'https://ams3.digitaloceanspaces.com/avtovin/media/mustang.jpeg',
      misc: null,
      order: 0,
      car_offers: []
    },
    {
      id: 4,
      name: 'Серьезные авто для серьёзных людей',
      title: 'Серьезные авто для серьёзных людей',
      collection_id: 'serious',
      photo: 'https://ams3.digitaloceanspaces.com/avtovin/media/mustang.jpeg',
      misc: null,
      order: 0,
      car_offers: []
    }
  ]
}
/* eslint-enable */

// ------------------------------------------------
// GET: Return Collections
// ------------------------------------------------
mock.onGet('/collection/collections/').reply(config => {
  // eslint-disable-next-line object-curly-newline
  const {
    page = 1,
    status = null,
    ordering = 'id',
    page_size = 10,
    search = ''
  } = config.params
  /* eslint-enable */

  const queryLowered = search.toLowerCase()
  const filteredData = data.collections.filter(
    collection =>
      /* eslint-disable operator-linebreak, implicit-arrow-linebreak */
      collection.title.toLowerCase().includes(queryLowered) &&
      collection.status === (status || collection.status)
  )
  /* eslint-enable  */

  const sortedData = filteredData.sort(sortCompare(ordering))
  if (ordering[0] == '-') sortedData.reverse()

  return [
    200,
    {
      results: paginateArray(sortedData, page_size, page),
      count: filteredData.length
    }
  ]
})

// ------------------------------------------------
// POST: Add new collection
// ------------------------------------------------

mock.onPost('/collection/collections').reply(config => {
  // Get event from post data
  const { collection } = JSON.parse(config.data)

  const { length } = data.collections
  let lastIndex = 0
  if (length) {
    lastIndex = data.collections[length - 1].id
  }
  collection.id = lastIndex + 1

  data.collections.push(collection)

  return [201, { collection }]
})

// ------------------------------------------------
// POST: Update Collection
// ------------------------------------------------

mock.onPut(/\/collection\/collections\/\d+/).reply(config => {
  // Get event from post data
  const { collection: collectionData } = JSON.parse(config.data)

  // Convert Id to number
  collectionData.id = Number(collectionData.id)

  const collection = data.collections.find(
    e => e.id === Number(collectionData.id)
  )
  Object.assign(collection, collectionData)

  return [201, { collection }]
})

// ------------------------------------------------
// DELETE: Remove Collection
// ------------------------------------------------
mock.onDelete(/\/collection\/collections\/\d+/).reply(config => {
  // Get collection id from URL
  let collectionId = config.url.substring(config.url.lastIndexOf('/') + 1)

  // Convert Id to number
  collectionId = Number(collectionId)

  data.collections = data.collections.filter(t => t.id == collectionId)

  return [200]
})

// ------------------------------------------------
// GET: Return Single collection
// ------------------------------------------------
mock.onGet(/\/collection\/collections\/\d+/).reply(config => {
  // Get event id from URL
  let collectionId = config.url.substring(config.url.lastIndexOf('/') + 1)

  // Convert Id to number
  collectionId = Number(collectionId)

  const collectionIndex = data.collections.findIndex(e => e.id === collectionId)
  const collection = data.collections[collectionIndex]

  if (collection) return [200, collection]
  return [404]
})
