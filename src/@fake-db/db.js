import mock from './mock'

/* eslint-disable import/extensions */

// JWT
import './jwt'

// Table
import './data/ecommerce'
// import './data/collection'
// import './data/offer'
// import './data/user'

/* eslint-enable import/extensions */

mock.onAny().passThrough() // forwards the matched request over network
