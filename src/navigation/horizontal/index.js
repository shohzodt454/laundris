export default [
  {
    header: 'Pages',
    icon: 'FileIcon',
    children: [
      {
        title: 'Home',
        route: 'home',
        icon: 'HomeIcon',
      },
      {
        title: 'Second Page',
        route: 'second-page',
        icon: 'FileIcon',
      },
      {
        title: 'Объявление',
        route: 'offers',
        icon: 'FileIcon',
      },
      {
        title: 'Подборки',
        route: 'collections',
        icon: 'FileIcon',
      },
    ],
  },
]
