export default [
  {
    title: 'verticalNav.home',
    route: 'home',
    icon: 'HomeIcon'
  },
  {
    title: 'verticalNav.customers',
    route: 'customers',
    icon: 'UsersIcon',
  },
  {
    title: 'verticalNav.inventory',
    route: 'inventory',
    icon: 'AnchorIcon'
  },
  {
    title: 'verticalNav.hotel',
    route: 'hotel-profile',
    icon: 'ServerIcon'
  },
  {
    title: 'verticalNav.reservations',
    route: 'reservations',
    icon: 'BookmarkIcon'
  },
]
