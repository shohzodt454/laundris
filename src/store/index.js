import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'

// Modules
import app from './app'
import auth from './auth'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'

Vue.use(Vuex)

// Secure local storage
var ls = new SecureLS({ isCompression: false })

export default new Vuex.Store({
  modules: {
    auth,
    app,
    appConfig,
    verticalMenu
  },
  plugins: [
    createPersistedState({
      storage: {
        getItem: key => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: key => ls.remove(key)
      }
    })
  ],
  strict: process.env.DEV
})
