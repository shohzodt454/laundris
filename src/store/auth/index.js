import axios from '@axios'

const initialState = () => ({
  accessToken: null,
  refreshToken: null,
  userData: null
})

export default {
  namespaced: true,
  state: initialState(),
  getters: {
    isAuthenticated(state) {
      return state.accessToken
    }
  },
  mutations: {
    SET_USER(state, val) {
      state.userData = val
    },
    SET_AUTH_TOKEN(state, { access, refresh }) {
      state.accessToken = access
      state.refresh = refresh
    },
    LOG_OUT(state) {
      Object.assign(state, initialState())
    }
  },
  actions: {
    async fetchUserData(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/v1/account/me/`)
          .then(response => {
            resolve(response)
          })
          .catch(error => reject(error))
      })
    },
    async loginUser(ctx, { username, password }) {
      return new Promise((resolve, reject) => {
        axios
          .post(`/v1/account/me/`, { username, password })
          .then(response => {
            resolve(response)
          })
          .catch(error => reject(error))
      })
    }
  }
}
