import store from '@/store'
import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchSnapshots(ctx, queryParams) {
      const userData = store.state.auth.userData
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/snapshot/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    // fetchUser(ctx, { id }) {
    //   return new Promise((resolve, reject) => {
    //     axios
    //       .get(`/account/users/${id}/`)
    //       .then(response => resolve(response))
    //       .catch(error => reject(error))
    //   })
    // },
    // addUser(ctx, userData) {
    //   return new Promise((resolve, reject) => {
    //     axios
    //       .post('/account/users/', { user: userData })
    //       .then(response => resolve(response))
    //       .catch(error => reject(error))
    //   })
    // }
  }
}
