import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { useUtils as useI18nUtils } from '@core/libs/i18n'

// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
const noop = () => { }

export default function useOffersList() {
    // Use toast
    const { t } = useI18nUtils()

    const toast = useToast()

    const refSnapshotsListTable = ref(null)

    // Table Handlers
    const tableColumns = [
        // { key: 'show-details', label: '', class: 'expand-col', sortable: false },
        { key: 'customer', label: 'Customer Item Name', sortable: false },
        { key: 'laundris_item_name', label: 'Laundris Item Name', sortable: false },
        { key: 'location', label: 'Location', sortable: false },
        { key: 'linen_quantity_in_usage', label: 'Room Qty', sortable: false },
        { key: 'linen_quantity_in_storage', label: 'Storage Qty', sortable: false },
        { key: 'linen_spoilage_quantity', label: 'Order Qty', sortable: false },
        { key: 'linen_quantity_at_laundris', label: 'Laundris Qty', sortable: false },
        { key: 'date', label: 'Snapshots Date', sortable: false },
    ]

    const snapshotsList = ref([])
    const perPage = ref(10)
    const totalSnapshots = ref(0)
    const currentPage = ref(1)
    const perPageOptions = [10, 25, 50, 100]
    const isLoading = ref(false)

    const dataMeta = computed(() => {
        const localItemsCount = refSnapshotsListTable.value
            ? refSnapshotsListTable.value.localItems.length
            : 0
        return {
            from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
            to: perPage.value * (currentPage.value - 1) + localItemsCount,
            of: totalSnapshots.value
        }
    })

    const refetchData = () => {
        refSnapshotsListTable.value.refresh()
    }

    watch(
        [currentPage, perPage],
        () => {
            refetchData()
        }
    )


    const fetchSnapshots = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-snapshot/fetchSnapshots', {
                page_size: perPage.value,
                page: currentPage.value,
            })
            .then(response => {
                const { snapshot_list, count } = response.data
                callback(snapshot_list)
                snapshotsList.value = snapshot_list
                totalSnapshots.value = count
                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Error fetching snapshots list',
                        icon: 'AlertTriangleIcon',
                        variant: 'danger'
                    }
                })
            })
    }


    return {
        fetchSnapshots,
        snapshotsList,
        totalSnapshots,
        isLoading,
        tableColumns,
        perPage,
        currentPage,
        dataMeta,
        perPageOptions,
        refSnapshotsListTable,
        t
    }
}
