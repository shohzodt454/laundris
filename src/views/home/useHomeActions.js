import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { useUtils as useI18nUtils } from '@core/libs/i18n'

// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
const noop = () => { }

export default function useOffersList() {
    // Use toast
    const { t } = useI18nUtils()

    const toast = useToast()

    const refDashboardPage = ref(null)


    const customersList = ref([])
    const statsList = ref([])
    const isLoading = ref(false)
    const customerFilter = ref(null)


    const refetchData = () => {
        fetchStats()
    }

    watch(
        [customerFilter],
        () => {
            refetchData()
        }
    )


    const fetchCustomers = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-dashboard/fetchCustomers', {
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                customersList.value = results
                customersList.value = results.map((item) => ({
                    value: item.customer_id,
                    label: item.customer_name,
                }));

                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
            })
    }

    const fetchStats = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-dashboard/fetchStats', {
                customer_name: customerFilter.value ? customerFilter.value : null,
            })
            .then(response => {
                callback(response.data)
                statsList.value = response.data
                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
            })
    }


    const resolveUserStatusVariant = status => {
        if (status === 'DRAFT') return 'secondary'
        if (status === 'ON_REVIEW') return 'warning'
        if (status === 'active') return 'success'
        if (status === 'inactive') return 'danger'
        return 'warning'
    }

    return {
        fetchCustomers,
        fetchStats,
        statsList,
        customersList,
        isLoading,
        resolveUserStatusVariant,
        customerFilter,
        refDashboardPage,
        t
    }
}
