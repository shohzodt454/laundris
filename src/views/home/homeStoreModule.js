import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchCustomers(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/customer/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchStats(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/statistics/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  }
}
