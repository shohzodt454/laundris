<template>
  <b-sidebar
    lazy
    id="sidebar-right"
    :visible="isSidebarActive"
    v-scroll-lock="isSidebarActive"
    width="25%"
    bg-variant="white"
    right
    backdrop
    shadow
    @change="(val) => $emit('update:is-sidebar-active', val)"
  >
    <!-- Table Container Card -->
    <b-card no-body class="mb-0">
      <div class="m-1">
        <h4 class="text-center" v-if="inventory">
          Edit Inventory #{{ inventory.customer_inventory_id }}
        </h4>

        <validation-observer ref="inventoryEditSidebar">
          <b-form ref="form" @submit.stop.prevent="onSubmit">
            <b-form-group label="Customer" class="mb-2">
              <validation-provider
                #default="{ errors }"
                v-model="customer"
                rules="required"
                name="customer"
                :reduce="(item) => item.value"
              >
                <v-select
                  v-model="customer"
                  :options="customersList"
                  placeholder="Customer"
                  :clearable="false"
                  class="genre-selector"
                  :reduce="(item) => item.value"
                />
                <small class="text-danger">{{ errors[0] }}</small>
              </validation-provider>
            </b-form-group>

            <b-form-group label="Contact" class="mb-2">
              <validation-provider
                #default="{ errors }"
                v-model="customer"
                rules="required"
                name="Contact"
                :reduce="(item) => item.value"
              >
                <v-select
                  v-model="contact"
                  :options="contactsList"
                  placeholder="Contact"
                  :clearable="false"
                  class="genre-selector"
                  :reduce="(item) => item.value"
                />
                <small class="text-danger">{{ errors[0] }}</small>
              </validation-provider>
            </b-form-group>

            <b-form-group label="Item Type" class="mb-2">
              <validation-provider
                #default="{ errors }"
                v-model="customer"
                rules="required"
                name="Item Type"
                :reduce="(item) => item.value"
              >
                <v-select
                  v-model="item_type"
                  :options="itemtypesList"
                  placeholder="Item Type"
                  :clearable="false"
                  class="genre-selector"
                  :reduce="(item) => item.value"
                />
                <small class="text-danger">{{ errors[0] }}</small>
              </validation-provider>
            </b-form-group>

            <b-form-group label="Location" class="mb-2">
              <b-form-input
                type="text"
                name="Location"
                v-model="customer_item_name"
                trim
              />
            </b-form-group>

            <b-form-group label="Room Qty" class="mb-2">
              <b-form-input
                type="text"
                name="Room Qty"
                v-model="linen_quantity_in_usage"
                trim
              />
            </b-form-group>

            <b-form-group label="Storage Qty" class="mb-2">
              <b-form-input
                type="text"
                name="Storage Qty"
                v-model="linen_quantity_in_storage"
                trim
              />
            </b-form-group>

            <b-form-group label="Order Qty" class="mb-2">
              <b-form-input
                type="text"
                name="Order Qty"
                v-model="linen_spoilage_quantity"
                trim
              />
            </b-form-group>

            <b-form-group label="Laundris Qty" class="mb-2">
              <b-form-input
                type="text"
                name="Laundris Qty"
                v-model="linen_quantity_at_laundris"
                trim
              />
            </b-form-group>

            <b-button variant="primary" type="submit">
              Edit Inventory
            </b-button>
          </b-form>
        </validation-observer>
      </div>
    </b-card>
  </b-sidebar>
</template>

<script>
import {
  BCard,
  BRow,
  BFormCheckbox,
  BCol,
  BFormInput,
  BButton,
  BImg,
  BImgLazy,
  VBTooltip,
  BSidebar,
  BForm,
  BFormGroup,
  BFormFile,
} from 'bootstrap-vue'
import vSelect from 'vue-select'
import store from '@/store'
import { ValidationProvider, ValidationObserver } from 'vee-validate'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import formValidation from '@core/comp-functions/forms/form-validation'
import { required } from '@validations'
export default {
  components: {
    BForm,
    BFormGroup,
    BFormCheckbox,
    BImg,
    BImgLazy,
    BCard,
    BRow,
    BCol,
    BFormInput,
    BButton,
    BSidebar,
    BFormFile,
    ValidationProvider,
    ValidationObserver,
    vSelect,
    required,
  },
  directives: {
    'b-tooltip': VBTooltip,
  },
  model: {
    prop: 'isSidebarActive',
    event: 'update:is-sidebar-active',
  },
  props: {
    isSidebarActive: {
      type: Boolean,
      required: true,
    },
    inventory: {
      default: null,
      required: true,
    },
    customersList: {
      default: [],
    },
    contactsList: {
      default: [],
    },
    itemtypesList: {
      default: [],
    },
  },
  data() {
    return {
      options: [0, 25, 50, 100, 150, 200],
      customer: null,
      contact: null,
      item_type: null,
      customer_item_name: null,
      linen_quantity_in_usage: null,
      linen_quantity_in_storage: null,
      linen_spoilage_quantity: null,
      linen_quantity_at_laundris: null,
    }
  },
  watch: {
    inventory(newValue) {
      if (newValue) {
        this.customer = this.inventory.customer.customer_id
        this.contact = this.inventory.contact.contact_id
        this.item_type = this.inventory.item_type.item_type_id
        this.customer_item_name = this.inventory.customer_item_name
        this.linen_quantity_in_usage = this.inventory.linen_quantity_in_usage
        this.linen_quantity_in_storage =
          this.inventory.linen_quantity_in_storage
        this.linen_spoilage_quantity = this.inventory.linen_spoilage_quantity
        this.linen_quantity_at_laundris =
          this.inventory.linen_quantity_at_laundris
        // store
        //   .dispatch(
        //     'app-inventory/fetchInventory',
        //     this.inventory.customer_inventory_id
        //   )
        //   .then((response) => {
        //     console.log(response.data)
        //   })
        //   .catch((err) => {})
      }
    },
  },
  methods: {
    onSubmit() {
      this.$refs.inventoryEditSidebar.validate().then((success) => {
        if (!success) return
        const reqData = {
          customer: parseInt(this.customer),
          contact: parseInt(this.contact),
          item_type: parseInt(this.item_type),
          customer_item_name: this.customer_item_name,
          linen_quantity_in_usage: parseInt(this.linen_quantity_in_usage),
          linen_quantity_in_storage: parseInt(this.linen_quantity_in_storage),
          linen_spoilage_quantity: parseInt(this.linen_spoilage_quantity),
          linen_quantity_at_laundris: parseInt(this.linen_quantity_at_laundris),
          linen_rfids_in_usage: [],
          linen_rfids_in_storage: [],
          linen_spoilage_rfids: [],
          linen_rfids_at_laundris: [],
        }

        store
          .dispatch('app-inventory/updateInventory', {
            id: this.inventory.customer_inventory_id,
            reqData,
          })
          .then(() => {
            this.$toast({
              component: ToastificationContent,
              props: {
                title: 'Inventory Edited succesfully!',
                icon: 'CheckIcon',
                variant: 'success',
              },
            })
            location.reload()
          })
          .catch((e) =>
            this.$toast({
              component: ToastificationContent,
              props: {
                title: `Error happened OOPS!`,
                icon: 'AlertTriangleIcon',
                variant: 'danger',
              },
            })
          )
          .finally(() => {
            // location.reload()
          })
      })
    },
  },
}
</script>

<style lang="scss">
@import '@core/scss/vue/libs/vue-select.scss';
</style>
