import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchInventories(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/inventory/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchCustomers(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/customer/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchContacts(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/contact/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchItemtypes(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/inventory/inventory-item-type/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addInventory(ctx, reqData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/v1/customer/inventory/', reqData)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateSnapshot(ctx, reqData) {
      return new Promise((resolve, reject) => {
        axios
          .post('/v1/customer/snapshot/', reqData)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateInventory(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .patch(`/v1/customer/inventory/${data.id}/`, data.reqData)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    }
  }
}
