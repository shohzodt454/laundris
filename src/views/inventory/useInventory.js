import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { useUtils as useI18nUtils } from '@core/libs/i18n'

// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
const noop = () => { }

export default function useOffersList() {
    // Use toast
    const { t } = useI18nUtils()

    const toast = useToast()

    const refInventoryListTable = ref(null)

    // Table Handlers
    const tableColumns = [
        // { key: 'show-details', label: '', class: 'expand-col', sortable: false },
        { key: 'customer', label: 'Customer Item Name', sortable: false },
        { key: 'item_type', label: 'Laundris Item Name', sortable: false },
        { key: 'location', label: 'Location', sortable: false },
        { key: 'linen_quantity_in_usage', label: 'Room Qty', sortable: false },
        { key: 'linen_quantity_in_storage', label: 'Storage Qty', sortable: false },
        { key: 'linen_spoilage_quantity', label: 'Order Qty', sortable: false },
        { key: 'linen_quantity_at_laundris', label: 'Laundris Qty', sortable: false },
        { key: 'actions', class: 'actions-col' }
    ]

    const inventoryList = ref([])
    const customersList = ref([])
    const contactsList = ref([])
    const itemtypesList = ref([])
    const perPage = ref(10)
    const totalInventory = ref(0)
    const currentPage = ref(1)
    const perPageOptions = [10, 25, 50, 100]
    const searchQuery = ref('')
    const isLoading = ref(false)
    const statusFilter = ref(null)

    const dataMeta = computed(() => {
        const localItemsCount = refInventoryListTable.value
            ? refInventoryListTable.value.localItems.length
            : 0
        return {
            from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
            to: perPage.value * (currentPage.value - 1) + localItemsCount,
            of: totalInventory.value
        }
    })

    const refetchData = () => {
        refInventoryListTable.value.refresh()
    }

    watch(
        [currentPage, perPage, searchQuery, statusFilter],
        () => {
            refetchData()
        }
    )


    const fetchInventories = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-inventory/fetchInventories', {
                page_size: perPage.value,
                page: currentPage.value,
                customer_id: statusFilter.value ? statusFilter.value : null,
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                inventoryList.value = results
                totalInventory.value = count
                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Error fetching inventory list',
                        icon: 'AlertTriangleIcon',
                        variant: 'danger'
                    }
                })
            })
    }

    const fetchCustomers = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-inventory/fetchCustomers', {
                page_size: perPage.value,
                page: currentPage.value,
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                customersList.value = results
                customersList.value = results.map((item) => ({
                    value: item.customer_id,
                    label: item.customer_name,
                }));

                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
            })
    }

    const fetchContacts = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-inventory/fetchContacts', {
                page_size: perPage.value,
                page: currentPage.value,
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                contactsList.value = results
                contactsList.value = results.map((item) => ({
                    value: item.contact_id,
                    label: item.first_name + ' ' + item.last_name,
                }));


                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
            })
    }

    const fetchItemtypes = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-inventory/fetchItemtypes', {
                page_size: perPage.value,
                page: currentPage.value,
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                itemtypesList.value = results
                itemtypesList.value = results.map((item) => ({
                    value: item.item_type_id,
                    label: item.item_name,
                }));

                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
            })
    }

    const updateSnapshot = reqData => {
        store
            .dispatch('app-inventory/updateSnapshot', reqData)
            .then(() => {
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Snapshgots Added succesfully!',
                        icon: 'CheckIcon',
                        variant: 'success',
                    },
                })
            })
            .catch((e) =>
                toast({
                    component: ToastificationContent,
                    props: {
                        title: `Error happened OOPS!`,
                        icon: 'AlertTriangleIcon',
                        variant: 'danger',
                    },
                })
            )
    }

    const resolveUserStatusVariant = status => {
        if (status === 'DRAFT') return 'secondary'
        if (status === 'ON_REVIEW') return 'warning'
        if (status === 'active') return 'success'
        if (status === 'inactive') return 'danger'
        return 'warning'
    }

    return {
        updateSnapshot,
        fetchCustomers,
        customersList,
        fetchInventories,
        inventoryList,
        fetchContacts,
        contactsList,
        fetchItemtypes,
        itemtypesList,
        isLoading,
        tableColumns,
        perPage,
        currentPage,
        totalInventory,
        dataMeta,
        perPageOptions,
        searchQuery,
        refInventoryListTable,
        resolveUserStatusVariant,
        statusFilter,
        t,
    }
}
