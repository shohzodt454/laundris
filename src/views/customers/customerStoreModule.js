import axios from '@axios'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchCustomers(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/customer/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchAddress(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/customer/address/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchUsers(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/v1/account/users/', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateCustomer(ctx, reqData) {
      return new Promise((resolve, reject) => {
        axios
          .patch(`/v1/customer/customer/${reqData.id}/`, reqData.data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    }
  }
}
