import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'
import { useUtils as useI18nUtils } from '@core/libs/i18n'

// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
const noop = () => { }

export default function useOffersList() {
    // Use toast
    const { t } = useI18nUtils()

    const toast = useToast()

    const refCustomersListTable = ref(null)

    // Table Handlers
    const tableColumns = [
        // { key: 'show-details', label: '', class: 'expand-col', sortable: false },
        { key: 'customer_name', sortable: false },
        { key: 'address', sortable: false },
        { key: 'active_flag', sortable: false },
        { key: 'actions', class: 'actions-col' }
    ]

    const customersList = ref([])
    const usersList = ref([])
    const addressList = ref([])
    const filteredCustomers = ref([])
    const perPage = ref(10)
    const totalCustomers = ref(0)
    const currentPage = ref(1)
    const perPageOptions = [10, 25, 50, 100]
    const searchQuery = ref('')
    const isLoading = ref(false)
    const statusFilter = ref(null)

    const dataMeta = computed(() => {
        const localItemsCount = refCustomersListTable.value
            ? refCustomersListTable.value.localItems.length
            : 0
        return {
            from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
            to: perPage.value * (currentPage.value - 1) + localItemsCount,
            of: totalCustomers.value
        }
    })

    const refetchData = () => {
        refCustomersListTable.value.refresh()
    }

    watch(
        [currentPage, perPage, searchQuery, statusFilter],
        () => {
            refetchData()
        }
    )


    const fetchCustomers = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-customer/fetchCustomers', {
                page_size: perPage.value,
                page: currentPage.value,
                search: searchQuery.value,
                active_flag: statusFilter.value ? statusFilter.value : 'active',
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                customersList.value = results
                filteredCustomers.value = results.map((item) => ({
                    value: item.customer_id,
                    label: item.customer_name,
                }));
                totalCustomers.value = count
                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Error fetching customers list',
                        icon: 'AlertTriangleIcon',
                        variant: 'danger'
                    }
                })
            })
    }

    const fetchAddress = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-customer/fetchAddress', {
                page_size: perPage.value,
                page: currentPage.value,
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                addressList.value = results
                addressList.value = results.map((item) => ({
                    value: item.customer_address_id,
                    label: item.address_type,
                }));
                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Error fetching address list',
                        icon: 'AlertTriangleIcon',
                        variant: 'danger'
                    }
                })
            })
    }

    const fetchUsers = (ctx = {}, callback = noop) => {
        isLoading.value = true

        store
            .dispatch('app-customer/fetchUsers', {
                page_size: perPage.value,
                page: currentPage.value,
            })
            .then(response => {
                const { results, count } = response.data
                callback(results)
                usersList.value = results
                usersList.value = results.map((item) => ({
                    value: item.user_id,
                    label: item.email,
                }));
                isLoading.value = false
            })
            .catch(e => {
                isLoading.value = false
                toast({
                    component: ToastificationContent,
                    props: {
                        title: 'Error fetching Users list',
                        icon: 'AlertTriangleIcon',
                        variant: 'danger'
                    }
                })
            })
    }


    //   const confirmOffer = id => {
    //     isConfirmLoading.value = id

    //     store
    //       .dispatch('app-offer/fetchOffers', { id })
    //       .then(response => {
    //         toast({
    //           component: ToastificationContent,
    //           props: {
    //             title: 'Объявление подтверждено',
    //             icon: 'CheckIcon',
    //             variant: 'success'
    //           }
    //         })
    //       })
    //       .catch(err => {
    //         toast({
    //           component: ToastificationContent,
    //           props: {
    //             title: 'Произошла ошибка в подтверждении',
    //             icon: 'AlertTriangleIcon',
    //             variant: 'danger'
    //           }
    //         })
    //       })
    //       .finally(() => (isConfirmLoading.value = null))
    //   }

    const resolveUserStatusVariant = status => {
        if (status === 'draft') return 'secondary'
        if (status === 'on_review') return 'warning'
        if (status === 'active') return 'success'
        if (status === 'inactive') return 'danger'
        return 'warning'
    }

    return {
        fetchAddress,
        fetchUsers,
        fetchCustomers,
        filteredCustomers,
        addressList,
        usersList,
        customersList,
        isLoading,
        tableColumns,
        perPage,
        currentPage,
        totalCustomers,
        dataMeta,
        perPageOptions,
        searchQuery,
        refCustomersListTable,
        resolveUserStatusVariant,
        statusFilter,
        t
    }
}
