import useJwt from '@core/auth/jwt/useJwt'
import axios from '@axios'

const config = {
  loginEndpoint: '/v1/account/me/',
  //   registerEndpoint: '/jwt/register',
  refreshEndpoint: '/v1/account/me/refresh/',
  //   logoutEndpoint: '/jwt/logout',

  // This will be prefixed in authorization header with token
  // e.g. Authorization: Bearer <token>
  tokenType: 'Bearer',

  // Value of this property will be used as key to store JWT token in storage
  storageTokenKeyName: 'access',
  storageRefreshTokenKeyName: 'refresh'
}

const { jwt } = useJwt(axios, config)
export default jwt
